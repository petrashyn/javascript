"use strict";

/*  Task 1  */

function ucFirst(userWord, str) {
        str = userWord.charAt(0).toUpperCase() + userWord.slice(1);
        alert(str);
    }

taskOne.onclick = function () {

    var userWord = prompt('Введите любое слово строчными буквами', '');

    if ((userWord === null) || (userWord === undefined)) {
        alert('Нет ошибок при пустой строке');
        return;
    }

    else if (!isNaN(userWord)) {
        alert('Первый символ - цифра, так нельзя!');
        return;
    }

    ucFirst(userWord);

}

//  -------------------------------------

/*  Task 2  */


 function minDetecting(i, j) {

    if (i >= j) {
        alert('Минимальное число = ' + j);
    }

    else {
        alert('Минимальное число = ' + i);
    }

 }


taskTwo.onclick = function () {

    var numberOne = +prompt('Введите первое число', '');
    var numberTwo = +prompt('Введите второе число', '');

    minDetecting(numberOne, numberTwo);

}

//  -------------------------------------

/*  Task 3  */

function numberIdentification(maxLenght, userNumber) {

    userNumber = +prompt('Введите проверочное число', '');

    // Проверка валидности числа сравнения

    if (userNumber === 0) {
        alert('Вы не задали диапазон');
        return;
    }

    else if (isNaN(userNumber)) {
        alert('Допускаются только числовые значения');
        return;
    }

    else {

    // Проверка и подбор числа выше диапазона

        while (userNumber < maxLenght) {

            userNumber = 0;

            userNumber++;

            userNumber = +prompt('Введенное число меньше диапазона вычислений, попробуйте еще раз', '');

        }
    }
}

taskThree.onclick = function () {

    var maxLenght = +prompt('Введите диапазон вычислений', '');

    // Проверка валидности диапазона

    if (maxLenght === 0) {
        alert('Вы не задали диапазон');
        return;
    }

    else if (isNaN(maxLenght)) {
        alert('Допускаются только числовые значения');
        return;
    }

    else if (maxLenght < 3) {
        alert('Заданный диапазон слишком мал');
        return;
    }

    // Если проверки пройдены - вызываем функцию для задания и валидации числа сравнения

    else {
        numberIdentification(maxLenght);
    }

}

//  -------------------------------------

/*  Task 4  */

// Функция определения области чисел для проверки

    function rangeDetecting(i, j) {

        for (; i <= j; i++) {

			primeNumbers(i);

        }
    }

// Функция отсеивания целых чисел

    function primeNumbers(i){

        for (var j = 2; j * j <= i; j++) {

            if (i % j == 0){

            	return false;
        	}

        console.log(i); // Вывод в консоль целых чисел

		}
    
    }

taskFour.onclick = function () {

    var minLenght = +prompt('Введите начальное число диапазона вычислений', '');
    var maxLenght = +prompt('Введите конечное число диапазона вычислений', '');

    // Проверка валидности диапазона

    if ((minLenght + maxLenght) == 0) {
        alert('Вы не задали диапазон');
        return;
    }

    else if (isNaN(minLenght) || isNaN(maxLenght)) {
        alert('Допускаются только числовые значения');
        return;
    }

    rangeDetecting(minLenght, maxLenght);

}