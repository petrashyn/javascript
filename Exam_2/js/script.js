// PORTFOLIO GALLERY (ISOTOPE)
$(document).ready(function() {

    var $isotopeGrid = $('.portfolio-images');

    $isotopeGrid.isotope({
        itemSelector: '.img-wrapper',
        percentPosition: true,
    });

    $('.portfolio .filters button').click(function() {
        var filterValue = $(this).data('filter');

        $isotopeGrid.isotope({
            filter: filterValue
        });
    });

    $('.portfolio-images').isotope({
        layoutMode: 'masonry',
        itemSelector: '.img-wrapper'
    });

    $('.grid').isotope({
        itemSelector: '.img-wrapper',
        masonry: {
            columnWidth: 100
        }
    });

    $('.gallery').on('click', function(e) {
        e.preventDefault();
        var element = $(this);

        if (!element.hasClass('active')) {
            element.addClass('active').css({});
            element.siblings('li').removeClass('active').css({});
        }
    })

});

// SLICK SLIDER ACTIVATION + SETTINGS

$(document).ready(function() {
    $(".single-item").slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: false,
        nextArrow: false
    });
});

// GOOGLE MAPS ADDON

function initMap() {
    var centerLatLng = new google.maps.LatLng(49.568583, 34.585416);
    var mapOptions = {
        center: centerLatLng,
        zoom: 15,
        disableDefaultUI: true
    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    // Добавляем маркер
    var marker = new google.maps.Marker({
        position: centerLatLng,
        map: map,
        title: "Our Address",
        icon: "img/map-pin.png"
    });
}

google.maps.event.addDomListener(window, "load", initMap);

// GOOGLE MAPS ADDON END


var $anchors = $('a[href^="#"]').not('[href="#"]');

$anchors.click(function(e) {
    e.preventDefault();

    var id = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(id).offset().top
    }, 600);
});