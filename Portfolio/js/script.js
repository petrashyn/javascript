// BOOTSTRAP MENU SCROLLSPY
$('body').scrollspy({ target: '#navbar' })

// WOW ANIMATION ACTIVATION
new WOW().init();

// SMOOTH SCROLL
var $anchors = $('a[href^="#"]').not('[href="#"]');

$anchors.click(function(e) {

    var id = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(id).offset().top
    }, 600);
});

// GOOGLE CHROME BG VIDEO AUTOPLAY
document.getElementsByTagName('video')[0].muted = true;
$(function() {
    setTimeout(function() {
        $("video")[0].play();
    }, 200);
})

// POJECTS GALLERY PLUGIN ACTIVATION
jQuery(function($) {
    $(document).on('click', '.lightboxgallery-gallery-item', function(event) {
        event.preventDefault();
        $(this).lightboxgallery({
            showCounter: true,
            showTitle: true,
            showDescription: true
        });
    });
});