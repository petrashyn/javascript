"use strict";

/*  Task 1  */

taskOne.onclick = function() {

    var arrayRange,
        randomNumber,
        randomArray = [];

    /* Генерируем массив из случайных чисел от 0 до заданного */
    arrayRange = +prompt('Введите число элементов для массива случайных чисел', '');

    for (var i = 0; i < arrayRange; i++) {
        randomNumber = Math.floor(Math.random() * arrayRange);
        randomArray.push(randomNumber);
    }

    /* Получаем результат по поставленной задаче */
    alert('Начальный массив: ' + randomArray);
    alert('Отфильтрованные четные в новом массиве: ' + map(isEven, randomArray));
    alert('Начальный массив остался без изменений: ' + randomArray);
}

/* Функция проверки на четность */
function isEven(x) {

    if (x % 2 === 0) {
        return true;
    }

    return false;
}

/* Функция объединения для нового массива */
function map(isEven, innerArray) {

    var arrayDublicate = []

    for (var i = 0; i < innerArray.length; i++) {

        if (isEven(innerArray[i]) == true) {
            arrayDublicate.push(innerArray[i]);
        }
    }

    return (arrayDublicate);
}

// ------------------------------

/*  Task 2  */

taskTwo.onclick = function() {

    var regexp = /^[0-9][0-9][0-9][0-9]$/g,
        customYear = prompt('Введите год для проверки', '');

    for (var i = 0;; i++) {

        if (customYear == customYear.match(regexp)) {
            customYear = +customYear;
            break;

        } else {
            customYear = prompt('Вы ввели неверные символы, попробуйте еще раз', '');
        }
    }

    if (checkingLeapYear(customYear) == true) {

        alert(customYear + ' - Високосный');

    } else {

        alert(customYear + ' - Невисокосный');
    }

}

// Фцнкция проверки на високосность
function checkingLeapYear(getYear) {
    return new Date(getYear, 1, 29).getDate() === 29;
}

// ------------------------------

/*  Task 3  */

taskThree.onclick = function() {

    var userNumbersText,
        userNumbersArray = [];

    userNumbersText = +prompt('Введите необходимое число', '');

    userNumbersArray = unfoldingNumber(userNumbersText);

    //Вывод результата разбития
    console.log('Единиц: ' + userNumbersArray[0]);
    console.log('Десятков: ' + userNumbersArray[1]);
    console.log('Сотен: ' + userNumbersArray[2]);
    console.log('Тысяч: ' + userNumbersArray[3]);
}

function unfoldingNumber(userNumbersText) {

    var digits = [];

    // Единицы
    digits[0] = Math.floor((userNumbersText / 1) % 10);

    // Десятки
    digits[1] = Math.floor((userNumbersText / 10) % 10);

    // Сотни
    digits[2] = Math.floor((userNumbersText / 100) % 10);

    // Tысячи
    digits[3] = Math.floor((userNumbersText / 1000) % 10);

    // Далее новые проверки можно добавлять до бесконечности..

    return digits;
}