"use strict";

/*  Task 1+2  */

taskOne.onclick = function () {

    var employeeName = [],
    	employeeSalary = []; 

		 for (var i = 0; ; i++) {       /* количество сотрудников неограничено */
			
			/* Последовательный ввод массивов людей и зарплат */
			employeeName.push (
				prompt('Введите имя ' + (i + 1) + ' сотрудника', '')
			);

			/* Выход из цикла ввода по нажатию Cancel или Esc */
			if (employeeName[i] == null) {
				employeeName.pop(i);
				break;
			}

			employeeSalary.push (
				+prompt('Введите его оклад', '')
			);				
		}

	/* Считаем общую ЗП */
    calculationSalarySumm(employeeSalary);

    /* Вычисляем самого состоятельного */
	calculationSalaryBoss(employeeName, employeeSalary);
}

/* Функция подсчета общей суммы зарплатного бюджета */
function calculationSalarySumm(salaryArray) {

    var salarySumm = 0;

        for (var i = 0; i < salaryArray.length; i++) {

            salarySumm += +salaryArray[i];
        }

    alert('Всего на зарплату уйдет ' + salarySumm + ' денег');
}

/* Вычисляем самого состоятельного */
function calculationSalaryBoss(name, salary) {

	var salaryTop = [],
		salaryName = {};

	/* Объединяем отдальные массивы имен и зарплат в один */
	for (var i = 0; i <= salary.length; i++) {

		salaryTop.push(salary[i]);
		salaryName[salary[i]] = name[i];
	}

	/* Сортируем и определяем */
 	salaryTop.sort(function(a, b) {
  		return b - a;
	});
 	
	/* Выводим результат */
	alert('Самый высокий оклад у ' + salaryName[salaryTop[0]] + ' : ' + salaryTop[0] );
}

//  -------------------------------------

/*  Task 3 - Homework  */

taskThree.onclick = function () {
	
	var automaticalArray = [];
	
	/* Функция генерации массива из 10 чисел. Ибо столько писать руками зная циклы негоже ))  */
	for (var i = 1; i <= 10; i++ ) {

		automaticalArray.push(i);
	}
	/* Получаем результат по поставленной задаче */
	alert('Начальный массив: ' + automaticalArray);
	alert('Новый массив: ' + map(square, automaticalArray));
	alert('Начальный массив остался без изменений: ' + automaticalArray);
}

/* Функция возведения в корень переданного числа */
function square(x) {
	return x * x;
}

/* Функция объединения для нового массива */
function map(toSquare, innerArray) {

	var arrayDublicate = []

	for (var i = 0; i < innerArray.length; i++) {

		arrayDublicate.push (
			toSquare(innerArray[i])
		);
	}

	return(arrayDublicate);
}