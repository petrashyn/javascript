"use strict";

/*  Task 1  */

taskOne.onclick = function () {

    let operandOne = +prompt('Введите первое число', '0');
    let operandTwo = +prompt('Введите второе число', '0');
    let operandsSumm = operandOne + operandTwo;

    if (isNaN(+operandsSumm)) {
        alert('Одно из ваших чисел - вовсе и не число. Прийдется значинать все заново.');
    }

    else {
        alert(
            'Сумма чисел: '
            + operandOne
            + ' + '
            + operandTwo
            + ' = '
            + operandsSumm
        );
    }

};

//  -------------------------------------

/*  Task 2.1  */

taskTwoOne.onclick = function () {

    let diceOne = 1 + Math.floor(Math.random() * 6);
    let diceTwo = 1 + Math.floor(Math.random() * 6);
    let diceSumm = +diceOne + +diceTwo;

    alert(
        'Полученные цифры '
        + '[ ' + diceOne + ' ]'
        + ' + '
        + '[ ' + diceTwo + ' ] '
        + ' = '
        + diceSumm
    );
};

//  -------------------------------------

/*  Task 2.2  */

taskTwoTwo.onclick = function () {

    let coinSide = 1 + Math.floor(Math.random() * 50);

    if (coinSide % 2 === 0) {
        alert('Орел');
    }

    else {
        alert('Решка');
    }

};

//  -------------------------------------

/*  Task 2.3  */

taskTwoThree.onclick = function () {

    let randomLimit = +prompt('Введите крайнее число рандомного диапазона', '50');

    if (isNaN(randomLimit)) {
        alert('Вы ввели недопустимый символ. Будьте внимательнее.');
    }

    else {
        let randomNumber = +Math.floor(Math.random() * randomLimit);
        alert(
            'Случайным числом оказалось: ' + randomNumber);
    }

};

//  -------------------------------------

/*  Task 2.4  */

taskTwoFour.onclick = function () {

    let randomStart = +prompt('Введите первое число рандомного диапазона', '0');
    let randomEnd = +prompt('Введите последнее число рандомного диапазона', '100');

    if ((isNaN(randomStart)) || (isNaN(randomEnd))) {
        alert('Вы ввели недопустимый символ. Будьте внимательнее.');
    }
    else {
        let randomNumber = randomStart + Math.floor(Math.random() * randomEnd);
        alert(
            'Случайным числом оказалось: ' + randomNumber);
    }

};


//  -------------------------------------

/*  Task 3  */

taskThree.onclick = function () {

    let rightName = 'ECMAScript';
    let whatName = prompt('Каково \"официальное\" название JavaScript?', '');

    if (whatName === rightName) {
        alert('Верно!');
    }

    else {
        alert('Не знаете? \"ECMAScript\"!');
    }

};

//  -------------------------------------

/*  Task 4  */

taskFour.onclick = function () {

    let a = +prompt('Введите число А', '0');
    let b = +prompt('Введите число B', '0');
    let result = ((a + b) < 4 ? 'мало' : 'много');

    alert(result);
};

//  -------------------------------------

/*  Task 5  */

taskFive.onclick = function () {

    let rightLogin = 'Админ';
    let rightPass = 'Черный Властелин';

    let userLogin = prompt('Введите Логин', '');

    if (userLogin === rightLogin) {

        let userPass = prompt('Введите Пароль', '');

        if (userPass === rightPass) {
            alert('Добро пожаловать!');
        }

        else if (userPass === null) {
            alert('Вход отменён');
        }

        else {
            alert('Я вас не знаю');
        }
    }

    else if (userLogin === null) {
        alert('Вход отменён');
    }

    else {
        alert('Я вас не знаю');
    }

};