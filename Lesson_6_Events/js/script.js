"use strict";

//Создаем базовый див
var divBase = document.createElement('div');
divBase.className = 'starting_div';
divBase.innerText = 'Press Ctr+E to Enter the Text';
divBase.title = 'Yep, just Ctr+E ))';
divBase.style.cursor = 'default';
document.body.appendChild(divBase);

// Создаем форму для textarea и прячем ее
var formCreation = document.createElement("form");
formCreation.action = "textarea.php";
formCreation.method = "post";
formCreation.style.display = "none";
formCreation.style.position = "relative";
document.body.appendChild(formCreation);

// Создаем textarea внутри формы
var textAreaCreation = document.createElement('textarea');
textAreaCreation.rows = "10";
textAreaCreation.cols = "50";
textAreaCreation.name = "text";
formCreation.appendChild(textAreaCreation);

// Создаем подпись под textarea
var textAreaHint = document.createElement('div');
textAreaHint.innerText = 'Press Ctrl+S to Save Your Text or ESC to Cancel and close the textarea';
textAreaHint.style.color = "#858585";
textAreaHint.style.position = "absolute";
textAreaHint.style.bottom = "-20px";
textAreaHint.style.left = "0px";
formCreation.appendChild(textAreaHint);

// Отслеживаем нажатие Ctr+E, прячем див и достаем текстовое поле, заглушив хоткеи браузеров
document.onkeydown = function() {

    // Появление теxtarea вместо пустого дива
    if (event.ctrlKey && event.keyCode == 69) {
        window.event.preventDefault(); // Блокируем хромовые дефолты на горячие клавиши
        window.event.stopPropagation(); // Блокируем дефолты мозиллы на горячие клавиши
        divBase.style.display = 'none';
        formCreation.style.display = 'block';
    }

    // Сброс если нажать ESC. Будет работать также и после заполнения с сохранением
    if (event.keyCode == 27) {
        formCreation.style.display = 'none';
        divBase.innerText = 'Press Ctr+E to Enter the Text';
        divBase.style.display = 'block';
    }

    // Внесение введенного в textarea текста в див
    if (event.ctrlKey && event.keyCode == 83) {
        window.event.preventDefault(); // Блокируем хромовые дефолты на горячие клавиши
        window.event.stopPropagation(); // Блокируем дефолты мозиллы на горячие клавиши
        divBase.innerHTML = 'Ваш введенный текст:' + textAreaCreation.value;
        formCreation.style.display = 'none';
        divBase.style.display = 'block';
        divBase.title = 'Yep, This is your text!';
    }
};